<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('uniqueId')->nullable();		
            $table->string('first_name');
            $table->string('last_name');
            $table->string('profile_pic')->default('/user/default.png')	;		  	  	
            $table->string('alt_email')->nullable();
            $table->enum('gender',['M','F']);
            $table->enum('gender_visible',['0','1'])->default('1');
            $table->date('dob');
            $table->enum('dob_visible',['0','1'])->default('1');
            $table->string('alt_mobile_no')->nullable();
            $table->string('temp_mobile_no')->nullable();
            $table->enum('email_verified',['0','1'])->default('0');
            $table->enum('mob_verified',['0','1'])->default('0');
            $table->string('about')->nullable();
            $table->string('work')->nullable();
            $table->string('education')->nullable();
            $table->enum('marital_status',['Single','Married'])->default('Single');
            $table->string('status');
            $table->enum('is_archieved',['0','1'])->default('0');
            $table->date('archieved_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_infos');
    }
}
