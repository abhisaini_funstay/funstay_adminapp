<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_info extends Model
{
    public function User(){
        return $this->belongsTo(User::class);
    }
    public function address(){
        return $this->belongsTo(address::class);
    }
}
