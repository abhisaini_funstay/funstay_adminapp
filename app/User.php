<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends \TCG\Voyager\Models\User
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'email', 'password','mobile', 'country_code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function tokens()
    {
        return $this->hasMany(Token::class);
    }

    
    public function getPhoneNumber()
    {
        return $this->country_code.$this->mobile;
    }

    
    public function user_type(){
        return $this->belongsTo(user_type::class);
    }
    public function user_info(){
        return $this->hasOne(user_info::class);
    }

//     public function roles() 
//     {
//    return $this->belongsToMany(App\Role::class);
//     }



}
